The webapp is written for backend and front end.
Backend is written in Java-SpringBoot.
Front End is written in ReactJS.

In order to run the webapp follow the instructions as below:

1. You should have a working internet connection at any point of time while executing the program.
2. Clone the repositiory in your local machine. (You require npm 'installed' in your ocal machine)
3. Open the code from an IDE (preferably IntelliJ).
4. Run the main method from UrlShortenerApplication.class.
5. If server starts succesfully, open terminal/command promt and cd to directory 'src/main/ui/short_url'.
6. Run the following commands (You require npm 'installed' in your ocal machine):
    npm install
    npm start
7. Once the server starts it will automatically redirect to http://localhost:3000 on a web browser.
8. Enjoy the webapp!